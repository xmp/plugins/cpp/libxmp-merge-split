#ifndef XMP_SPLIT_OPERATOR_H
#define XMP_SPLIT_OPERATOR_H

#include <vector>

#include <xmp/plugin/cpp/OperatorBase.h>
#include <xmp/core/operator/OperatorParameters.h>

class SplitOperator : public xmp::plugin::OperatorBase
{
public:
    SplitOperator(std::string const &id,
                  xmp::core::OperatorPriority priority,
                  xmp::core::IOperatorParameters const *parameters,
                  xmp::core::IContext *context);
    virtual ~SplitOperator();

    void execute();
    bool start();
    bool stop();

private:
    std::vector<xmp::core::IOutput *> outputs;
    xmp::core::IInput *input;
};

#endif //XMP_SPLIT_OPERATOR_H
