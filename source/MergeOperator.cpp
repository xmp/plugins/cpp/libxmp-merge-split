#include "MergeOperator.h"

#include <sstream>
#include <cstring>

#include <xmp/plugin/cpp/LogMaker.h>

MergeOperator::MergeOperator(
        std::string const &id,
        xmp::core::OperatorPriority priority,
        xmp::core::IOperatorParameters const *parameters,
        xmp::core::IContext *context)
        : xmp::plugin::OperatorBase(id, priority, context),
          output(NULL),
          currentInputIndex(0)
{
    bool hasErrors = false;
    std::size_t inputsCount = 0;
    const char *inputsCountStr = parameters->getParameter("inputs_count");
    if (inputsCountStr != NULL) {
        if (strlen(inputsCountStr) == 1) {
            if (isdigit(inputsCountStr[0])) {
                inputsCount = atoi(inputsCountStr);
            }
            else {
                xmp::LogMaker::error(context) << "Invalid 'inputs_count' parameter value '" << inputsCountStr
                                              << "' for merge operator with name '" << id << "'"
                                              << std::endl;
                hasErrors = true;
            }
        } else {
            inputsCount = atoi(inputsCountStr);
            if (inputsCount == 0 || inputsCount < 0) {
                xmp::LogMaker::error(context) << "Invalid 'inputs_count' parameter value '" << inputsCountStr
                                              << "' for merge operator with name '" << id << "'"
                                              << std::endl;
                hasErrors = true;
            }
        }
    } else {
        xmp::LogMaker::error(context) << "No value provided for 'inputs_count' parameter "
                                      << "for merge operator with name '" << id << "'"
                                      << std::endl;
        hasErrors = true;
    }

    if (hasErrors) {
        xmp::LogMaker::fatal(context) << "Failed to create merge operator with name '" << id << "'"
                                      << std::endl;
    } else {
        for (std::size_t i = 0; i < inputsCount; i++) {
            std::stringstream stream;
            stream << i;
            std::string inputName = "in" + stream.str();
            inputs.push_back(registerInput(inputName.c_str()));
        }

        output = registerOutput("out", xmp::core::ON_CHANGE);
    }
}

MergeOperator::~MergeOperator()
{
}

void MergeOperator::execute()
{
    std::size_t stepsCount = 0;
    do {
        currentInputIndex++;
        if (currentInputIndex >= inputs.size()) {
            currentInputIndex = 0U;
        }

        xmp::core::IBuffer *buffer = inputs[currentInputIndex]->read();
        if (buffer != NULL) {
            buffer->pushFront(static_cast<char>(currentInputIndex));
            output->write(buffer);
            break;
        }

        stepsCount++;
    } while (stepsCount != inputs.size());
}

bool MergeOperator::start()
{
    return true;
}

bool MergeOperator::stop()
{
    return true;
}
