#ifndef XMP_MERGE_OPERATOR_H
#define XMP_MERGE_OPERATOR_H

#include <vector>

#include <xmp/plugin/cpp/OperatorBase.h>
#include <xmp/core/operator/OperatorParameters.h>

class MergeOperator : public xmp::plugin::OperatorBase
{
public:
    MergeOperator(std::string const &id,
                  xmp::core::OperatorPriority priority,
                  xmp::core::IOperatorParameters const *parameters,
                  xmp::core::IContext *context);
    virtual ~MergeOperator();

    void execute();
    bool start();
    bool stop();

private:
    std::vector<xmp::core::IInput *> inputs;
    xmp::core::IOutput *output;

    std::size_t currentInputIndex;
};

#endif //XMP_MERGE_OPERATOR_H
