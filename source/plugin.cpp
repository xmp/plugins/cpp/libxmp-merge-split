#include <xmp/plugin/cpp/plugin.h>

#include <xmp/plugin/cpp/GenericOperatorCreator.h>

#include "PriorityMergeOperator.h"
#include "MergeOperator.h"
#include "SplitOperator.h"

void registerCreators(xmp::core::IOperatorStore *store)
{
    store->registerCreator("priority_merge", new xmp::plugin::GenericOperatorCreator<PriorityMergeOperator>("print"));
    store->registerCreator("merge", new xmp::plugin::GenericOperatorCreator<MergeOperator>("print"));
    store->registerCreator("split", new xmp::plugin::GenericOperatorCreator<SplitOperator>("print"));
}
