#include "PriorityMergeOperator.h"

#include <sstream>
#include <cstring>

#include <xmp/plugin/cpp/LogMaker.h>

PriorityMergeOperator::PriorityMergeOperator(
        std::string const &id,
        xmp::core::OperatorPriority priority,
        xmp::core::IOperatorParameters const *parameters,
        xmp::core::IContext *context)
        : xmp::plugin::OperatorBase(id, priority, context),
          output(NULL)
{
    bool hasErrors = false;
    std::size_t inputsCount = 0;
    const char *inputsCountStr = parameters->getParameter("inputs_count");
    if (inputsCountStr != NULL) {
        if (strlen(inputsCountStr) == 1) {
            if (isdigit(inputsCountStr[0])) {
                inputsCount = atoi(inputsCountStr);
            }
            else {
                xmp::LogMaker::error(context) << "Invalid \'inputs_count\' parameter value '" << inputsCountStr
                                              << "' for priority_merge operator with name '" << id << "'"
                                              << std::endl;
                hasErrors = true;
            }
        }
        else {
            inputsCount = atoi(inputsCountStr);
            if (inputsCount == 0 || inputsCount < 0) {
                xmp::LogMaker::error(context) << "Invalid \'inputs_count\' parameter value '" << inputsCountStr
                                              << "' for priority_merge operator with name '" << id << "'"
                                              << std::endl;
                hasErrors = true;
            }
        }
    } else {
        xmp::LogMaker::error(context) << "No value provided for \'inputs_count\' parameter "
                                      << "for priority_merge operator with name '" << id << "'"
                                      << std::endl;
        hasErrors = true;
    }

    if (hasErrors) {
        xmp::LogMaker::fatal(context) << "Failed to create priority_merge operator with name '" << id << "'"
                                      << std::endl;
    } else {
        for (std::size_t i = 0; i < inputsCount; i++) {
            std::stringstream stream;
            stream << i;
            std::string inputName = "in" + stream.str();
            inputs.push_back(registerInput(inputName.c_str(), xmp::core::NO_DISPATCH));
        }

        output = registerOutput("out", xmp::core::ON_EMPTY);
    }
}

PriorityMergeOperator::~PriorityMergeOperator()
{
}

void PriorityMergeOperator::execute()
{
    if (output->isEmpty())
    {
        for (std::size_t i = 0; i < inputs.size(); i++) {
            xmp::core::IBuffer *buffer = inputs[i]->read();
            if (buffer)  {
                buffer->pushFront(static_cast<char>(i));
                output->write(buffer);
                break;
            }
        }
    }
}

bool PriorityMergeOperator::start()
{
    return true;
}

bool PriorityMergeOperator::stop()
{
    return true;
}
