#include "SplitOperator.h"

#include <sstream>
#include <cstring>

#include <xmp/plugin/cpp/LogMaker.h>

SplitOperator::SplitOperator(
        std::string const &id,
        xmp::core::OperatorPriority priority,
        xmp::core::IOperatorParameters const *parameters,
        xmp::core::IContext *context)
        : xmp::plugin::OperatorBase(id, priority, context),
          input(NULL)
{
    bool hasErrors = false;
    std::size_t outputsCount = 0;
    const char *outputsCountStr = parameters->getParameter("outputs_count");
    if (outputsCountStr != NULL) {
        if (strlen(outputsCountStr) == 1) {
            if (isdigit(outputsCountStr[0])) {
                outputsCount = atoi(outputsCountStr);
            } else {
                xmp::LogMaker::error(context) << "Invalid \'outputs_count\' parameter value '" << outputsCountStr
                                              << "' for split operator with name '" << id << "'"
                                              << std::endl;
                hasErrors = true;
            }
        } else {
            outputsCount = atoi(outputsCountStr);
            if (outputsCount == 0 || outputsCount < 0) {
                xmp::LogMaker::error(context) << "Invalid \'outputs_count\' parameter value '" << outputsCountStr
                                              << "' for split operator with name '" << id << "'"
                                              << std::endl;
                hasErrors = true;
            }
        }
    } else {
        xmp::LogMaker::error(context) << "No value provided for \'outputs_count\' parameter "
                                      << "for split operator with name '" << id << "'"
                                      << std::endl;
        hasErrors = true;
    }

    if (hasErrors) {
        xmp::LogMaker::fatal(context) << "Failed to create split operator with name '" << id << "'"
                                      << std::endl;
    } else {
        input = registerInput("in");

        for (std::size_t i = 0; i < outputsCount; i++) {
            std::stringstream stream;
            stream << i;
            std::string outputName = "out" + stream.str();
            outputs.push_back(registerOutput(outputName.c_str()));
        }
    }
}

SplitOperator::~SplitOperator()
{
}

void SplitOperator::execute()
{
    xmp::core::IBuffer *buffer = input->read();
    if (buffer) {
        if (buffer->size() != 0) {
            std::size_t outputIndex = static_cast<std::size_t>(buffer->at(0));
            if (outputIndex < outputs.size()) {
                buffer->popFront();
                outputs[outputIndex]->write(buffer);
            }
            else {
                xmp::LogMaker::fatal(getContext()) << "Get invalid output index from buffer for "
                                                           << "split operator with name '" << getId() << "'"
                                                           << std::endl;
            }
        }
    }
}

bool SplitOperator::start()
{
    return true;
}

bool SplitOperator::stop()
{
    return true;
}
