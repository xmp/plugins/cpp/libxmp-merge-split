#ifndef XMP_PRIORITY_MERGE_OPERATOR_H
#define XMP_PRIORITY_MERGE_OPERATOR_H

#include <vector>

#include <xmp/plugin/cpp/OperatorBase.h>
#include <xmp/core/operator/OperatorParameters.h>

class PriorityMergeOperator : public xmp::plugin::OperatorBase
{
public:
    PriorityMergeOperator(std::string const &id,
                          xmp::core::OperatorPriority priority,
                          xmp::core::IOperatorParameters const *parameters,
                          xmp::core::IContext *context);
    virtual ~PriorityMergeOperator();

    void execute();
    bool start();
    bool stop();

private:
    std::vector<xmp::core::IInput *> inputs;
    xmp::core::IOutput *output;

    std::size_t inputsCount;
};

#endif //XMP_PRIORITY_MERGE_OPERATOR_H
